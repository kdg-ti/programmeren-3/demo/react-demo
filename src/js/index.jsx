import '../css/style.css'

import * as React from 'react'
import * as ReactDOM from 'react-dom'

function Paragraph() {
    return (
        <p>
            This is a <strong>paragraph</strong>!
        </p>
    )
}

ReactDOM.render(Paragraph(), document.getElementById('root'))
